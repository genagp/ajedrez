var rows;
var cols;
var mat = new Array(8);
var lugarInicial

for (i = 0; i < 8; i++) {
    mat[i] = new Array(8);
}

for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
        if (i == 1 ) {
            mat[i][j] = 11;
        }else if(i == 6){ 
            mat[i][j] = 21;
        }else if(i==0){
            mat[i][0] = 12;
            mat[i][1] = 13;
            mat[i][2] = 14;
            mat[i][3] = 15;
            mat[i][4] = 16;
            mat[i][5] = 14;
            mat[i][6] = 13;
            mat[i][7] = 12;
        }else if(i == 7){
            mat[i][0] = 22;
            mat[i][1] = 23;
            mat[i][2] = 24;
            mat[i][3] = 25;
            mat[i][4] = 26;
            mat[i][5] = 24;
            mat[i][6] = 23;
            mat[i][7] = 22;
        }else{
            mat[i][j] = 99;
        }
    }
}

console.log(mat)

function selectPieceB(ID){
    document.getElementById(ID).style.backgroundColor = 'green'
    movimientos(ID)
    return false;
}

function movimientos(ID){
    imgsB = false
    imgsN = false
    Vacio = false
    var idInicial = 0
    lugarAMover = ""
    console.log(document.getElementById(ID).outerHTML)

    for (i = 0; i < document.getElementsByClassName('imgsB').length; i++) { 
        if (document.getElementsByClassName('imgsB')[i].style.backgroundColor == 'green' && document.getElementsByClassName('imgsB')[i].id !== ID) {
            console.log("entre al 1")
            imgsB = true;
            idInicial = document.getElementsByClassName('imgsB')[i].id
            break;
        }
    }
    for (i = 0; i < document.getElementsByClassName('imgsN').length; i++) { 
        if (document.getElementsByClassName('imgsN')[i].style.backgroundColor == 'green' && document.getElementsByClassName('imgsN')[i].id !== ID) {
            console.log("entre al 2")
            imgsN = true;
            idInicial = document.getElementsByClassName('imgsN')[i].id
            break;
        }
    }
    for (i = 0; i < document.getElementsByClassName('Vacio').length; i++) { 
        if (document.getElementsByClassName('Vacio')[i].style.backgroundColor == 'green' && document.getElementsByClassName('Vacio')[i].id !== ID) {
            console.log("entre al 3")
            Vacio = true;
            idInicial = document.getElementsByClassName('Vacio')[i].id
            break;
        }
    }
    if (imgsB == true || imgsN == true || Vacio == true) {
        lugarAMover = document.getElementById(ID).outerHTML

        document.getElementById(ID).outerHTML = lugarInicial
        document.getElementById(idInicial).outerHTML = lugarAMover

        for (i = 0; i < document.getElementsByClassName('imgsB').length; i++) { 
            document.getElementsByClassName('imgsB')[i].style.backgroundColor = '' 
        }
        for (i = 0; i < document.getElementsByClassName('imgsN').length; i++) { 
            document.getElementsByClassName('imgsN')[i].style.backgroundColor = '' 
        }
        for (i = 0; i < document.getElementsByClassName('Vacio').length; i++) { 
            document.getElementsByClassName('Vacio')[i].style.backgroundColor = '' 
        }
        console.log('todo piola')
        console.log(lugarAMover)
        lugarInicial = ""
    }else{
        console.log('guardando')
        lugarInicial = document.getElementById(ID).outerHTML
        console.log(lugarInicial)
    }

    

}

function selectPieceN(ID){

    document.getElementById(ID).style.backgroundColor = 'green'
    movimientos(ID)
    return false;
}


function createTable(rows, cols) {

    var table = document.createElement('table');

    table.setAttribute('border', '1');
    table.setAttribute('id','tablero')

    for (let i = 0; i < rows; i++) {
        var tRow = document.createElement('tr');
        
        for (let j = 0; j < cols; j++) {
            var tData = document.createElement('td'); 

            if (j%2 !== 0 && i%2 == 0) {
                tData.style.backgroundColor = "brown";
            }else if (j%2 == 0 && i%2 !== 0) {
                tData.style.backgroundColor = "brown";
            }else{
                tData.style.backgroundColor = "lightgrey";
            }

            /* tData.onclick = function(){
                for (i = 0; i < document.getElementsByClassName('imgsB').length; i++) { 
                    if (document.getElementsByClassName('imgsB')[i].style.backgroundColor == 'green') {
                        console.log(document.getElementsByClassName('imgsB')[i])
                    }
                }
            } */

            switch (i) {
                case 0:
                    if (j == 0) {
                        tData.innerHTML = '<img id="TBI" class = "imgsB" src="../public/imgs/TB.png" onclick="selectPieceB(\'TBI\');" alt="">';
                    }

                    if (j == 1) {
                        tData.innerHTML = '<img id="CBI" class = "imgsB" src="../public/imgs/CB.png" onclick="selectPieceB(\'CBI\');" alt="">';

                    }

                    if (j == 2) {
                        tData.innerHTML = '<img id="ABI" class = "imgsB" src="../public/imgs/AB.png" onclick="selectPieceB(\'ABI\');" alt="">';

                    }

                    if (j == 3) {
                        tData.innerHTML = '<img id="RB" class = "imgsB" src="../public/imgs/RB.png" onclick="selectPieceB(\'RB\');" alt="">';

                    }

                    if (j == 4) {
                        tData.innerHTML = '<img id="QB" class = "imgsB" src="../public/imgs/QB.png" onclick="selectPieceB(\'QB\');" alt="">';

                    }

                    if (j == 5) {
                        tData.innerHTML = '<img id="ABD" class = "imgsB" src="../public/imgs/AB.png" onclick="selectPieceB(\'ABD\');" alt="">';

                    }

                    if (j == 6) {
                        tData.innerHTML = '<img id="CBD" class = "imgsB" src="../public/imgs/CB.png" onclick="selectPieceB(\'CBD\');" alt="">';
                    }

                    if (j == 7) {
                        tData.innerHTML = '<img id="TBD" class = "imgsB" src="../public/imgs/TB.png" onclick="selectPieceB(\'TBD\');" alt="">';

                    }
                    break;
                case 1:
                    if (j == 0) {
                        tData.innerHTML = '<img id="P0" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P0\');" alt="">';
                    }

                    if (j == 1) {
                        tData.innerHTML = '<img id="P1" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P1\');" alt="">';
                    }

                    if (j == 2) {
                        tData.innerHTML = '<img id="P2" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P2\');" alt="">';
                    }

                    if (j == 3) {
                        tData.innerHTML = '<img id="P3" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P3\');" alt="">';
                    }

                    if (j == 4) {
                        tData.innerHTML = '<img id="P4" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P4\');" alt="">';
                    }

                    if (j == 5) {
                        tData.innerHTML = '<img id="P5" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P5\');" alt="">';
                    }

                    if (j == 6) {
                        tData.innerHTML = '<img id="P6" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P6\');" alt="">';
                    }

                    if (j == 7) {
                        tData.innerHTML = '<img id="P7" class = "imgsB" src="../public/imgs/PB.png" onclick="selectPieceB(\'P7\');" alt="">';
                    }
                    break;
                case 2:
                    switch (j){

                        case 0:
                            tData.innerHTML = '<img id="V0" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V0\');" alt="">';
                            break;
                        case 1:
                            tData.innerHTML = '<img id="V1" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V1\');" alt="">';
                            break;
                        case 2:
                            tData.innerHTML = '<img id="V2" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V2\');" alt="">';
                            break;
                        case 3:
                            tData.innerHTML = '<img id="V3" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V3\');" alt="">';
                            break;
                        case 4:
                            tData.innerHTML = '<img id="V4" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V4\');" alt="">';
                            break;
                        case 5:
                            tData.innerHTML = '<img id="V5" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V5\');" alt="">';
                            break;
                        case 6:
                            tData.innerHTML = '<img id="V6" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V6\');" alt="">';
                            break;
                        case 7:
                            tData.innerHTML = '<img id="V7" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V7\');" alt="">';
                            break;
                        default:
                            break;
                        }
                        break;
                case 3:
                    switch (j){
                        case 0:
                            tData.innerHTML = '<img id="V8" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V8\');" alt="">';
                            break;
                        case 1:
                            tData.innerHTML = '<img id="V9" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V9\');" alt="">';
                            break;
                        case 2:
                            tData.innerHTML = '<img id="V10" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V10\');" alt="">';
                            break;
                        case 3:
                            tData.innerHTML = '<img id="V11" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V11\');" alt="">';
                            break;
                        case 4:
                            tData.innerHTML = '<img id="V12" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V12\');" alt="">';
                            break;
                        case 5:
                            tData.innerHTML = '<img id="V13" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V13\');" alt="">';
                            break;
                        case 6:
                            tData.innerHTML = '<img id="V14" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V14\');" alt="">';
                            break;
                        case 7:
                            tData.innerHTML = '<img id="V15" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V15\');" alt="">';
                            break;
                        default:
                            break;                
                    }
                    break;
                case 4:
                    switch (j){
                        case 0:
                            tData.innerHTML = '<img id="V16" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V16\');" alt="">';
                            break;
                        case 1:
                            tData.innerHTML = '<img id="V17" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V17\');" alt="">';
                            break;
                        case 2:
                            tData.innerHTML = '<img id="V18" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V18\');" alt="">';
                            break;
                        case 3:
                            tData.innerHTML = '<img id="V19" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V19\');" alt="">';
                            break;
                        case 4:
                            tData.innerHTML = '<img id="V20" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V20\');" alt="">';
                            break;
                        case 5:
                            tData.innerHTML = '<img id="V21" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V21\');" alt="">';
                            break;
                        case 6:
                            tData.innerHTML = '<img id="V22" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V22\');" alt="">';
                            break;
                        case 7:
                            tData.innerHTML = '<img id="V23" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V23\');" alt="">';
                            break;
                        default:
                            break;
                    }
                    break;
                case 5:
                    switch (j){
                        case 0:
                            tData.innerHTML = '<img id="V24" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V24\');" alt="">';
                            break;
                        case 1:
                            tData.innerHTML = '<img id="V25" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V25\');" alt="">';
                            break;
                        case 2:
                            tData.innerHTML = '<img id="V26" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V26\');" alt="">';
                            break;
                        case 3:
                            tData.innerHTML = '<img id="V27" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V27\');" alt="">';
                            break;
                        case 4:
                            tData.innerHTML = '<img id="V28" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V28\');" alt="">';
                            break;
                        case 5:
                            tData.innerHTML = '<img id="V29" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V29\');" alt="">';
                            break;
                        case 6:
                            tData.innerHTML = '<img id="V30" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V30\');" alt="">';
                            break;
                        case 7:
                            tData.innerHTML = '<img id="V31" class = "Vacio" src="../public/imgs/Vacio.png" onclick="selectPieceN(\'V31\');" alt="">';
                            break;
                        default:
                            break;
                    }
                    break;
                case 6:
                    if (j == 0) {
                        tData.innerHTML = '<img id="P10" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P10\');" alt="">';
                    }

                    if (j == 1) {
                        tData.innerHTML = '<img id="P11" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P11\');" alt="">';
                    }

                    if (j == 2) {
                        tData.innerHTML = '<img id="P12" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P12\');" alt="">';
                    }

                    if (j == 3) {
                        tData.innerHTML = '<img id="P13" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P13\');" alt="">';
                    }

                    if (j == 4) {
                        tData.innerHTML = '<img id="P14" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P14\');" alt="">';
                    }

                    if (j == 5) {
                        tData.innerHTML = '<img id="P15" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P15\');" alt="">';
                    }

                    if (j == 6) {
                        tData.innerHTML = '<img id="P16" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P16\');" alt="">';
                    }

                    if (j == 7) {
                        tData.innerHTML = '<img id="P17" class = "imgsN" src="../public/imgs/PN.png" onclick="selectPieceN(\'P17\');" alt="">';
                    }
                    break;
                case 7:
                    if (j == 0){
                        tData.innerHTML = '<img id="TNI" class = "imgsN" src="../public/imgs/TN.png" onclick="selectPieceN(\'TNI\');" alt="">';
                    }

                    if (j == 1) {
                        tData.innerHTML = '<img id="CNI" class = "imgsN" src="../public/imgs/CN.png" onclick="selectPieceN(\'CNI\');" alt="">';
                    }

                    if (j == 2) {
                        tData.innerHTML = '<img id="ANI" class = "imgsN" src="../public/imgs/AN.png" onclick="selectPieceN(\'ANI\');" alt="">';
                    }

                    if (j == 3) {
                        tData.innerHTML = '<img id="QN" class = "imgsN" src="../public/imgs/QN.png" onclick="selectPieceN(\'QN\');" alt="">';
                    }

                    if (j == 4) {
                        tData.innerHTML = '<img id="RN" class = "imgsN" src="../public/imgs/RN.png" onclick="selectPieceN(\'RN\');" alt="">';
                    }

                    if (j == 5) {
                        tData.innerHTML = '<img id="AND" class = "imgsN" src="../public/imgs/AN.png" onclick="selectPieceN(\'AND\');" alt="">';
                    }

                    if (j == 6) {
                        tData.innerHTML = '<img id="CND" class = "imgsN" src="../public/imgs/CN.png" onclick="selectPieceN(\'CND\');" alt="">';
                    }

                    if (j == 7) {
                        tData.innerHTML = '<img id="TND" class = "imgsN" src="../public/imgs/TN.png" onclick="selectPieceN(\'TND\');" alt="">';
                    }    
                    break;                
                default:
                    break;
            }

            tRow.appendChild(tData);

        }

        table.appendChild(tRow);
    }

    document.body.appendChild(table);
}

createTable(8, 8);